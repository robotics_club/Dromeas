/*

  "▀▓╣╣╣▓▓▓▓▄▄▄╖,                                                               
     ╙▓╣╣╣╣╣╣╣╣╣╣╣╣▓▓▄▄╖,,                                                      
       ╙▓╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣▓▓▓▓▓▓▄▄▄▄▄▄▄▄▄▄╖╖╖╖╖╖╖╖╖╖,,,,,                     
         ▀╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣▓▓▓▄▄╖,           
          ╙╣▓▓▓▓▓╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣▓▓▄,      
           ╓▄▓▓▓▄▄┌╙▓╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣▓▄,   
         ▄╣╣╣╣╣╣╣╣╣▓,╙╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣▓, 
        ▓╣╣╣╣╣╣╣╣╣╣╣╣ ╘╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣▓
        ╣╣╣╣╣╣╣╣╣╣╣╣╣L ╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣
        ╚╣╣╣╣╣╣╣╣╣╣╣▓ ,╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣╣
         ╙▓╣╣╣╣╣╣╣▓▀                                                    ╚▓▓▓▓▓▓▓
            '╙"""                                                         "▀▀▀` 

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        DROMEAS                                             //
//                                 LINE FOLLOWING PROJECT                                     //
//                          Robotics Club - University of Patras                              // 
//                          http://robotics.mech.upatras.gr/club/                             //
//                          https://www.facebook.com/Polymechanon/                            //
//                                                                                            //
// - Microcontroller: Arduino Nano                                                            //
// - Motor Driver   : TB6612FNG(Pololu)                                                       //
// - Line Sensors   : QTR-8RC(Pololu)                                                         //
//                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////

*/

#include "Arduino.h"
#include <QTRSensors.h>

//-----------------------PIN DEFINITIONS-----------------------------//

//IR Sensors
#define QTR_1 0
#define QTR_2 1
#define QTR_3 2
#define QTR_4 3
#define QTR_5 4
#define QTR_6 5
#define QTR_7 6
#define QTR_8 7

//Motor Driver
#define PWMA 9
#define PWMB 10
#define AIN1 8
#define AIN2 11
#define STBY 12
#define BIN1 13
#define BIN2 14

//LEDs
#define RED_LED 15
#define YELLOW_LED 16
#define BLUE_LED 17

//-----------------------ALGORITHM COEFFICIENTS-----------------------------//

//Sensors Library
#define NUM_SENSORS   8    // number of sensors used
#define TIMEOUT       1000  // waits for 1000 microseconds for sensor outputs to go low
#define EMITTER_PIN   QTR_NO_EMITTER_PIN     // emitters always on

//QTR sensor object
QTRSensorsRC qtrrc((unsigned char[]) {QTR_1 , QTR_2 , QTR_3 , QTR_4 , QTR_5 , QTR_6 , QTR_7, QTR_8}, NUM_SENSORS, TIMEOUT, EMITTER_PIN);

//Linear Velocity
#define MAX_VOLT 220 // PWM output max limit to protect motor
int linear_speed = 0; // Assigned new value during setup

//PID CONTROL
#define PID_REF 0
float Kp = 0.0, Ki=0.0, Kd = 0.0; //PID parameters, assigned new value during setup

#define LOOP_FREQUENCY 3 //in milliseconds

//-----------------------ALGORITHM VARIABLES-----------------------------//

unsigned int sensorValues[NUM_SENSORS];  //sensors' callibrated readen values 0-1000
int sensorColour[NUM_SENSORS];          //sensors' readen colours  0-1
bool online[NUM_SENSORS];
bool on_line = false; // false: all sensors see font
                      // true: at least one sensor sees line 
float pid_error_derivative=0.0;
int reflectance_output=0;     //pid - angle decision {[-350,350], 1000, -1000}
int angle_detection;
int pid_error, pid_error_prev=0, pid_error_integral=0;  
unsigned long previousTime=0, currentTime; //keep track of actual execution time
int linear_vel, angular_vel, angular_turn;
static int last_value=0; // assume initially that the line is left.
int PA=0, PB=0;


//-----------------------------------FUNCTIONS-------------------------------------//

// Setup: Runs only once
void setup() {

  Serial.begin(115200);

  //Motor Driver 
  pinMode(PWMA, OUTPUT);
  pinMode(PWMB, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(STBY,OUTPUT);

  //LEDs
  pinMode(RED_LED,OUTPUT);
  pinMode(BLUE_LED,OUTPUT);
  pinMode(YELLOW_LED,OUTPUT);

  //Varriables initialization
  linear_speed=110; //max speed with line centered

  Kp=0.46;
  Kd=0.67;

  angular_turn=130; // angular velocity used in right or left angle
  
  digitalWrite(STBY,HIGH); // Motor Driver State - Active
  delay(2000); // waiting time to start sensor calibration
  digitalWrite(BLUE_LED, HIGH); // calibration start

  for (int i = 0; i < 500; i++)  // make the calibration take about 10 seconds
  {
    qtrrc.calibrate();       // reads all sensors 10 times at 2500 us per read (i.e. ~25 ms per call)
  }
  digitalWrite(BLUE_LED, LOW); // calibration over
  delay(3000); // waiting time before motors start 
  // Detect Color of line on starting point
  read_sensors(); 
} //end setup

// Read Sensors: Function used to read the value of each sensor from the object and normalise them between 0 and 1000
void read_sensors() { //read from all sensors

  //read reflectance sensors
  qtrrc.read(sensorValues); //get new values
  for (int i=0; i<NUM_SENSORS ; i++) {
    //project sensor raw values , into a given range (0-1000)
    sensorColour[i] = map(sensorValues[i], qtrrc.calibratedMinimumOn[i]+20 , qtrrc.calibratedMaximumOn[i] , 0 , 1000 );
    if (sensorColour[i]<0)
    sensorColour[i]=0;
    if (sensorColour[i]>1000)
    sensorColour[i]=1000;
  }
}

// CheckOnline: Function used to check if robot is over line
void CheckOnline() {

  for(int i=0;i<NUM_SENSORS;i++){ // Loop through each sensor
    int value = sensorColour[i]; // Read the value of each sensor
    // keep track of whether we see the line at all
    if(value > 400){ 
      on_line = true;
      online[i] = true;
    }
    else {
      online[i] = false;
    }//end if
  }//end for
}

// Find line: Function used to estimate the position of the robot towards the line and calculate the value to use for PD Control
int find_line() {

  //from pololu read_line
  long avg=0; // this is for the weighted total, which is long before division
  int sum=0; // this is for the denominator which is <= 64000
  on_line = false; // false: all sensors see font
                   // true: at least one sensor sees line 

  CheckOnline(); // Function call

  for(int i=0;i<NUM_SENSORS;i++){
    int value = sensorColour[i];
    /* For DEBUG purpose ONLY
       Print the value of each sensor or the online vector
       Do not uncomment both lines at the same time, may be difficult to read
       Do not leave uncommented during run or it will affect loop frequency */
    // Serial.print(value); Serial.print("\t");
    // Serial.print(online[i]); Serial.print('\t');

    //!!!!ATTENTION!!!!!
    //The expected average (for QTR-8RC) would be 0-7000 , with reference point = 3500 
    if(value > 30) {  // only average in values that are above a noise threshold
      avg += (long)(value) * (i * 1000);
      sum += value;
    }
  }  

  // Serial.println(); // Uncomment when any one of the print lines above is uncomented to add a new line on each loop

  //************************* CHECK LINE VISIBILITY ****************************//

  // ************ LINE NOT AVAILABLE ************
  if(!on_line){ // No sensor sees line, all sensors see font

    // RIGHT ANGLE   
    if(angle_detection==1) {
      //Serial.println("right angle"); // Used for debug, leave commented during run
      return 1000; //special case (no line available)
    }
    // LEFT ANGLE
    else if(angle_detection==-1) {
      //Serial.println("right angle"); // Used for debug, leave commented during run
      return -1000; //special case (no line available)
    }
  } 


  // ************ LINE AVAILABLE ************ 
  else { // Line is available

		//*************SENSOR'S HISTORY**************//
		  
		// check if the right/left sensor is on the line
		// needed to distinguish right angle from dashed line
		if (online[0]) {
		  angle_detection=-1;
		} 
		else if (online[NUM_SENSORS-1]) {
		  angle_detection=1;
		}
		else {
		  angle_detection=0;
		}
	}

	// ************* REGULAR LINE *************
	last_value = avg/sum;
	// Value is calculated from 0 to 7000, 3500 means the line is in the middle
	// Derive it by ten to make it from 0 to 700 and substract 350 so now when the value is 0
	// means the line is in the middle
	return last_value/10-350; //normalise to [-350,+350]
	//end line is visible
}


// PID Control: Function used to run PID algorithm and return the result
int pid_control() {
  pid_error = PID_REF+reflectance_output;
  pid_error_integral+=pid_error*LOOP_FREQUENCY;      //calculate integral
  pid_error_derivative = 10.0*(float)(pid_error-pid_error_prev)/(float)LOOP_FREQUENCY; //calculate derivative (sampling time is considered)
  pid_error_prev = pid_error; //save the last value of the error (for next iteration)
  int pid_output = pid_error*Kp + pid_error_integral*Ki + pid_error_derivative*Kd; // Even though we calculate the integral since Ki = 0 we dont take it into account                      
  return pid_output;
}

// Command motors: Function to calculate the speed and direction of each motor
void command_motors(int v, int th) { //v is forward velocity, th is angular velocity
  PA =-v - th;
  PB = v - th;

  // Saturate within [-255,+255].
  if (PA < -MAX_VOLT) {
    PA=-MAX_VOLT;
  }
  else if (PA > MAX_VOLT) { 
    PA=MAX_VOLT;
  }
  if (PB > MAX_VOLT) {
    PB=MAX_VOLT;
  }
  else if (PB < -MAX_VOLT) {
    PB=-MAX_VOLT;
  }

  // The signs of PA, PB determine the direction of the motors
  // If a motor turns to the wrong direction simply change operator from >= to <= and vice versa
  if (PA>=0) {
      digitalWrite (AIN1,HIGH);
      digitalWrite (AIN2,LOW);
  }
  else {     
      digitalWrite (AIN1,LOW);
      digitalWrite (AIN2,HIGH);
  } 
  if (PB>=0) {
      digitalWrite (BIN1,LOW);
      digitalWrite (BIN2,HIGH);
  }
  else {
      digitalWrite (BIN1,HIGH);
      digitalWrite (BIN2,LOW);
  }
  // Write the values to the motors
  analogWrite(PWMA,abs(PA));
  analogWrite(PWMB,abs(PB));
 
}

// Process: Function that processes the result of find_line and calls the appropriate function
void process(int reflectance_output) {
  if (reflectance_output<=350 && reflectance_output>=-350){  //robot on-line
    linear_vel = linear_speed;
    angular_vel = pid_control();
  }
  else {  //robot lost line
    linear_vel=0;
    if (reflectance_output == 1000) {
      //right turn
      //Serial.println("handling right angle"); // Used for debug, leave commented during run
     angular_vel=angular_turn;  
    }
    else {
      //left turn
       //Serial.println("handling right angle"); // Used for debug, leave commented during run
      angular_vel=-angular_turn;      
    }
  }
}

/////////////////////////////////////////////////////   LOOP
void loop() {
   
  currentTime = millis(); //get current time
  if (currentTime - previousTime > LOOP_FREQUENCY) { //accurate timekeeping, makes the sure the code doesn't run in less than LOOP_FREQUENCY
    previousTime = currentTime; //save the last time we entered the loop
    //read data from all sensors
    read_sensors(); 
    //estimate robot position towards line
    reflectance_output = find_line(); 
    //Serial.println(reflectance_output); // Used for debug, leave commented during run
    //calculate linear and angular velocity
    process(reflectance_output);
    //drive the motors with the Appropriate Pulse
    command_motors(linear_vel, angular_vel);
    //timekeeper
  }
}