/*
  Robotics Club 2019-2020
  University of Patras, Greece
  Timer V4.0
  This code works with ESP32, it uses 2 sensors to meassure lap time and speed. 
  The LCD screen show the best lap time from power up, the latest lap time and the speed in m/s and km/h 
*/

#include <LiquidCrystal.h>

// Define sensor pins
#define phototransistor_1 33
#define phototransistor_2 25

// Define LCD pins
#define rs 5
#define en 18
#define d4 19
#define d5 21
#define d6 22
#define d7 23


#define phototransistors_distance 0.13 // The distance between the 2 sensors, 13cm

LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// Varriables used to keep track of time
volatile unsigned long time1 = 0;
volatile unsigned long time2 = 0;
unsigned long laptime = 0, current_time = 0;

// Varriables used to control when the interrupts store the current time
volatile bool speed = true;
volatile bool lap = true;

// Varriables used to store speed and time
float ms = 0.0, km = 0.0;
float bestlap = 500.0, prevlap = 0.0, dt = 0.0;



void setup() {

  Serial.begin(115200); // Initialize serial, used to debug
  pinMode(phototransistor_1, INPUT);
  attachInterrupt(digitalPinToInterrupt(phototransistor_1), interrupt1, FALLING); // This interrupt is used only to meassure speed
  pinMode(phototransistor_2, INPUT);
  attachInterrupt(digitalPinToInterrupt(phototransistor_2), interrupt2, FALLING); // This interrupt is used to meassure lap time and with coordination to interrupt1 meassures speed

  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(4,0);
  lcd.print("UPatras");
  lcd.setCursor(2,1);
  lcd.print("RoboticsClub");

  delay(1000);
  for (int positionCounter = 0; positionCounter < 16; positionCounter++) {
    lcd.scrollDisplayLeft();
    delay(200);
  }
  lcd.clear();
  lcd.setCursor(5,0);
  lcd.print("Ready!");
  delay(2000);
}

void interrupt1() {
  if (speed == true) {
    time1 = millis(); // Only store the current time when speed varriable is true
  }
  speed = false; // Make speed varriable false to only keep the first time the robot breaks the sensor line for this lap
}

void interrupt2() {
  if (lap == true) {
    time2 = millis(); // Only store the current time when lap varriable is true
  }
  lap = false; // Make lap varriable false to only keep the first time the robot breaks the sensor line for this lap
}

void loop() {

  if(lap == false && speed == false){ // If both lap and speed are false calculate the time difference between the 2 interrupts
    dt = fabs(time1 - time2); // Calculate the absolute value of difference, since they are floats fabs() is used instead of abs()
    dt = dt/1000.0; // Convert ms to s
    ms = phototransistors_distance/dt; // Calculate speed in m/s
    km = ms*3.6; // Convert m/s to km/h
  }
  if(lap == false && (millis() - time2 > 2000)){ // If lap is false and at least 2s have passed since the time was acquired meassure lap time
    prevlap = float(time2 - laptime)/1000.0; // Calculate the time difference from the last time the sensor line was broken in s
    laptime = time2; // Store the current time the sensor line is broken to use in the next lap
    lap = true; // Restore lap varriable to true to enable storing the time from the interrupt
  }
  if(prevlap <= bestlap && prevlap != 0.0){ // If this lap time is better than the best lap replace best lap with current
    bestlap = prevlap;
  }
  if(millis() - time1 > 2000){ // If 2 s have passed since the time the interrupt1 stored the time make speed varriable true again
    speed = true;
  }

  // Show speed and lap time on the LCD
  if(millis() - current_time >= 200){ // Refresh the lcd every 200 millis
    lcd.clear();
    lcd.setCursor(10,0);
    lcd.print("L:");
    lcd.print(prevlap);
    lcd.setCursor(0,0);
    lcd.print("Best:");
    lcd.print(bestlap);
    lcd.setCursor(9,1);
    lcd.print("m/s:");
    lcd.print(ms);
    lcd.setCursor(0,1);
    lcd.print("km:");
    lcd.print(km);
  }
}